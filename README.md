# Introduction
mygpoclient-java is my attempt to port the Python [mygpoclient](http://gpodder.org/mygpoclient/) to Java. I've tried to stay as close to the original Python source as possible so that developers that are comfortable with the Python version will be at home.
The [gPodder.net](http://gpodder.net) web service provides a way to manage podcasts and keep them synchronized across multiple devices. It is used in the [gPodder desktop client](http://gpodder.org).
This port to Java is in support of my [Podder client for Android](http://podder.dragontek.com).

# Recent Milestones
- 20160121 - Migrated repository to GitLab
- 20120819 - Interfaces added for standardized IFeeds and IPodcasts
- 20120819 - FeedServiceClient supports http://feeds.gpodder.net
- 20120816 - Added iTunesSearchApi and GoogleReaderApi to "extras" package.
- 20120718 - Transitioned to Gson library and removed Json.org dependency.
- 20120713 - FeedService implemented
- 20120604 - Project converted to Maven for dependency management.
- 20120530 - Original push to GitHub

# Quick Start
This project uses [Maven](http://maven.apache.org) to manage dependencies and the build process.

If you plan to use Eclipse for development, you can create an Eclipse project with Maven:
```
$ git clone https://gitlab.com/dragontek/mygpoclient-java.git
$ cd mygpoclient-java/mygpoclient-java
$ mvn eclipse:eclipse
```
To generate javadocs with Maven. From that same directory:
```
$ mvn javadoc:javadoc
$ firefox target/site/apidocs/index.html
```
To run the tests, you'll need to create a `client.properties` file in the root folder of the project with `username`, `password` and `deviceId` values in it.
WARNING: These tests will connect to the gPodder.net service, please don't abuse.
```
$ vi client.properties
$ mvn test
```
NOTE: The client.properties file is in the .gitignore file so that it doesn't get pushed to the repository!
Finally, to package for use in your application:
```
$ mvn package -Dmaven.test.skip=true
```
Or if your project uses maven already, you can install it into your Maven repository:
```
$ mvn install -Dmaven.test.skip=true
```
# Authors and Contributors
Lead Developer - Josh Mondragon @dragontek.

The original Python project, created by Thomas Perl @thp can also be found on GitHub https://github.com/gpodder/mygpoclient.

The documentation for the gPodder.net API Webservice can be found here: http://wiki.gpodder.org/wiki/Web_Services/API

# Support or Contact
Please feel free to post any issues here: https://github.com/Dragontek/mygpoclient-java/issues or contact support@dragontek.com.